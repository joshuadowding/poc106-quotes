package com.configureone.api.plugin.poc106.configuration;

import com.configureone.api.configuration.*;
import com.configureone.api.core.Concept;
import com.configureone.api.sales.LineItem;
import com.configureone.api.sales.quote.Quote;
import concept.jpoc.ConfigurableField;
import concept.jpoc.Description;
import org.apache.log4j.Logger;

/**
 * Generated Java Class Skeleton for JPoC type = DES_PRE_EVALUATE (extends com.configureone.api.configuration.JpocConfigurationRuleEngine)
 * Generation 3
 * @date Fri Mar 05 09:02:43 CST 2021
 * @author Joshua Dowding (jdowding)
 * @version 1.0-SNAPSHOT
 * @file SetQuoteSequenceNumber.java
 **/
public class SetQuoteSequenceNumber extends com.configureone.api.configuration.JpocConfigurationRuleEngine {
    private static final Logger logger = Logger.getLogger(SetQuoteSequenceNumber.class);

    private boolean debug;
    private static final String CONFIG_INPUT = "QUOTE_SEQ_NO";

    @Override
    public java.lang.Void execute() {
        try {
            String origin = getConfiguration().getOriginatingContextType(); // Where did we come from?
            logOutput("Configuration: '" + getConfiguration().getSerialNumber() + "'");

            if (origin.equals("quote")) {
                Quote quote = getConfiguration().getOriginatingQuote();

                // Check if line already exists:
                boolean found = false;
                for (LineItem line : quote.getLineItems()) {
                    if (line.isConfiguration()) {
                        String lineSerial = line.getAsConfigurationLineItem().getConfiguration().getSerialNumber();
                        if (lineSerial.equals(getConfiguration().getSerialNumber())) {
                            logOutput("Configuration: '" + getConfiguration().getSerialNumber() + "' already on quote.");
                            found = true;
                        }
                    }
                }

                if (!found) {
                    int lineNum = (quote.getLineItems().size() + 1);
                    setInput(CONFIG_INPUT, Integer.toString(lineNum));
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return null;
    }

    private boolean setInput(String input, String value) throws Exception {
        ConfigurationInput configInput = getConfiguration().getInputByName(input);
        SetSingleValuedInput sci =
                Concept.getInstance()
                        .getFactory()
                        .getConfigurationCommandFactory()
                        .createSetSingleValuedInputCommand(configInput);
        sci.setValue(value);
        sci.execute();
        return true;
    }

    @ConfigurableField
    @Description("true to debug and false for no logging.")
    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    private void logOutput(String text) {
        if (debug) {
            logger.info(text);
        }
    }
}
